package VisualGame2D;

//java game making tutorial
//this is not my work - mostly just following a youtube tutorial
//left off at http://www.youtube.com/watch?v=CZ6i8RM6Uxg

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class VisualGame2D extends JFrame implements Runnable, MouseMotionListener{
	
	private int x, y, xDirection, yDirection, mX, mY;
	private Image dbImage;
	private Graphics dbGraphics;
	private Image face;
	private boolean mouseOnScreen;
	private boolean mouseDragged;
	
	public void run(){
		try{
			while(true){
				move();
				Thread.sleep(5);
			}
		}
		catch(Exception e){
			System.out.println("Error");
		}
		
	}
	
	public void move(){
		x += xDirection;
		y += yDirection;
		if(x <= 0)
			x = 0;
		if(x >= 480)
			x = 480;
		if(y <= 0)
			y = 0;
		if( y >= 480)
			y = 480;
	}
	public void setXDirection(int xDir){
		xDirection = xDir;
	}
	public void setYDirection(int yDir){
		yDirection = yDir;
	}
	public class Mouse extends MouseAdapter{
		@Override
		public void mousePressed(MouseEvent e){
			int xCoord = e.getX();
			int yCoord = e.getY();
			x = xCoord-10;
			y = yCoord-10;
		}
		@Override
		public void mouseReleased(MouseEvent e){
			
		}
		@Override
		public void mouseEntered(MouseEvent e){
			mouseOnScreen = true;
		}
		@Override
		public void mouseExited(MouseEvent e){
			mouseOnScreen = false;
		}
	}
	
	public class AL extends KeyAdapter {
		public void keyPressed(KeyEvent e){
			int keyCode = e.getKeyCode();
			if(keyCode == e.VK_LEFT){
				setXDirection(-1);
			}
			if(keyCode == e.VK_RIGHT){
				setXDirection(+1);
			}
			if(keyCode == e.VK_UP){
				setYDirection(-1);
				
			}
			if(keyCode == e.VK_DOWN){
				setYDirection(+1);
			}
			
		}
		public void keyReleased(KeyEvent e){
			int keyCode = e.getKeyCode();
			if(keyCode == e.VK_LEFT){
				setXDirection(0);
			}
			if(keyCode == e.VK_RIGHT){
				setXDirection(0);
			}
			if(keyCode == e.VK_UP){
				setYDirection(0);
				
			}
			if(keyCode == e.VK_DOWN){
				setYDirection(0);
			}
		}
	}
	
	
	public VisualGame2D(){
		//Load Images
		ImageIcon faceIcon = new ImageIcon("C:/Users/Yol Bolsun/programming/eclipse/workspace/VisualGame2D/src/VisualGame2D/face.gif");
		face = faceIcon.getImage();
		
		//Game Properties
		addKeyListener(new AL());
		addMouseListener(new Mouse());
		addMouseMotionListener(this);
		setTitle("VisualGame2D");
		setSize(500, 500);
		setResizable(false);
		setVisible(true);
		setBackground(Color.GREEN);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		x = 150;
		y = 150;
	}
	public void paint(Graphics g){
		dbImage = createImage(getWidth(), getHeight());
		dbGraphics = dbImage.getGraphics();
		paintComponent(dbGraphics);
		g.drawImage(dbImage, 0, 0, this);
	}
	public void paintComponent(Graphics g){
		g.setColor(Color.RED);
		//g.fillOval(x, y, 15, 15);
		g.drawImage(face, x, y, this);
		if(mouseOnScreen){
			g.drawString("Coord: (" + x+ ", " + y + ")", 150, 150);
		}
		/*if(mouseDragged){
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(mX,  mY, 20, 20);
		}else{
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(Color.DARK_GRAY);
			g.fillRect(mX,  mY, 20, 20);
		}*/
		repaint();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VisualGame2D game = new VisualGame2D();
		Thread t1 = new Thread(game);
		t1.start();
	}
	@Override
	public void mouseDragged(MouseEvent e){
		mX = e.getX()-10;
		mY = e.getY()-10;
		mouseDragged = true;
		e.consume();
	}
	@Override
	public void mouseMoved(MouseEvent e){
		mX = e.getX()-10;
		mY = e.getY()-10;
		mouseDragged = false;
		e.consume();
	}
}
